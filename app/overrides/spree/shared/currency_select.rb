Deface::Override.new( :virtual_path => "spree/shared/_main_nav_bar/currency_selector.html.erb.deface",
                      :remove => "li#currency-set",
                      :name => "remove_currency_select",
                      :disabled => false)
Deface::Override.new( :virtual_path => "spree/shared/_filters",
                      :replace => "erb[silent]:contains('filters = @taxon ? @taxon.applicable_filters : [Spree::Core::ProductFilters.all_taxons]')",
					  :text => "<% filters = @taxon ? @taxon.applicable_filters(session[:currency]) : [Spree::Core::ProductFilters.all_taxons] %>",
                      :name => "currency_filter",
					  :disabled => false)
Deface::Override.new( :virtual_path => "spree/shared/_filters",
                      :insert_after => "erb[loud]:contains('hidden_field_tag')",
					  :text => "<%= hidden_field_tag 'currency', session[:currency] %>",
                      :name => "currency_filter2",
					  :disabled => false)
