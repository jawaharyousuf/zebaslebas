# This migration comes from spree_zebas_theme (originally 20141222192010)
class AddZoneIdToSpreePaymentMethods < ActiveRecord::Migration
  def change
    add_column :spree_payment_methods, :zone_id, :decimal, default: 1
  end
end
