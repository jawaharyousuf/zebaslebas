require "yaml"
require "koala"

class Product
	attr_accessor :sku,:start_sku, :end_sku, :name, :description, :price_inr, :price_cad, :categories, :image_path, :slug, :album
end
def setup_fb
	oauth_access_token="CAACEdEose0cBAEf376FedpNpCcAgpjCencJaKJmU7mpXxe4BLGqbaaTJEEyrbdonGUt5HZBjR5ZCOVIAbajCZAZChCGw8djlNRbRFYbX0CyhQskv72gMCc2uOIZCZBSXrp7qBjETZA77GiZCAQbH6u3y7LB8GLoji9QkmY0EO1IDqPZApWk3d4ZCllF4oHDPip1JGBETgGxH1YAadbxzpUZB84ddwHoG0m9B0QZD"
	graph = Koala::Facebook::API.new(oauth_access_token)
	pages = graph.get_connections("me", "accounts")
	page_token = pages.first['access_token']
	@page_graph= Koala::Facebook::API.new(page_token)
	albums = @page_graph.get_connections("me", "albums")
	@albumhash={}
	albums.each do |album|
		@albumhash[album["name"]]=album["id"]
	end
end
def upload_to_fb(product)
	if @albumhash[product.album].nil?
		newalbum=@page_graph.put_object('me','albums', :name=>product.album)
		@albumhash[product.album]=newalbum["id"]
	end

	picture_desc = "#{product.description}\n\nCode: #{product.sku}\n"
	picture_desc = picture_desc + "Price: Rs #{product.price_inr}/- \n" if product.price_inr
	#picture_desc = picture_desc + "Price: $#{product.price_inr} CAD \n" if product.price_cad
	picture_desc = picture_desc + "\nTo order WhatsApp/SMS 9940578247 or email zebaslebas@gmail.com or inbox us.\n"
	product.image_path.each do |image_file|
		@page_graph.put_picture(image_file, { "message" => picture_desc }, @albumhash[product.album])
	end
end
def upload_to_db(products, upload_to_fbk)
	taxons={}
	setup_fb if upload_to_fbk
	properties = Spree::Prototype.first.properties
	products.each do |product|
		puts product.inspect
		if Spree::Product.find_by_slug(product.slug).nil?
			spree_product = {
				:name => product.name,
				:description => product.description,
				:shipping_category_id => 1,
				:price => product.price_cad,
				:slug => product.slug,
				:sku => product.sku
			}
			if product.price_cad 
				spree_product[:available_on] = Time.zone.now
			end
			spree_product = Spree::Product.create!(spree_product)
			if product.price_inr
				price = spree_product.price_in("INR")
				price.price=product.price_inr
				price.save!
			end
			if product.price_cad
				price = spree_product.price_in("CAD")
				price.price=product.price_cad
				price.save!
			end
			product.categories.split("|").each do |taxon_name|
				taxons[taxon_name]||= Spree::Taxon.find_by_name(taxon_name)
				spree_product.taxons << taxons[taxon_name]
			end
			product.image_path.each do |image_file|
				image = Spree::Image.create!(:attachment => File.open(image_file))
				spree_product.images << image
			end
			spree_product.properties << properties
			if upload_to_fbk
				upload_to_fb(product)
			end
		else
			puts "Slug exists!"
			if upload_to_fbk
				upload_to_fb(product)
				sleep 3
			end
		end
	end
end

namespace :loadyaml do
	task :products => :environment do
		#Spree::Product.destroy_all
		products = YAML.load_file("#{Rails.root}/config/products.yml")
		upload_to_db(products, true)
	end
	task :album => :environment do
		#Spree::Product.destroy_all
		catalog = YAML.load_file("#{Rails.root}/config/album.yml").first
		products = []
		(catalog.start_sku..catalog.end_sku).each do |sku|
			product = Product.new
			product.name = catalog.name
			product.sku = sku
			product.description = catalog.description
			product.price_cad = catalog.price_cad
			product.price_inr = catalog.price_inr
			product.categories = catalog.categories
			product.image_path = [catalog.image_path.first+"#{sku}.jpg"]
			product.slug = "#{catalog.album.downcase}_#{sku}"
			product.album = catalog.album
			products << product
		end
		upload_to_db(products, true)
	end	
end