# Set the working application directory
# working_directory "/path/to/your/app"
working_directory "/home/spree/zebaslebas"

# Unicorn PID file location
# pid "/path/to/pids/unicorn.pid"
pid "/home/spree/zebaslebas/unicorn.pid"

# Path to logs
# stderr_path "/path/to/log/unicorn.log"
# stdout_path "/path/to/log/unicorn.log"
stderr_path "/home/spree/zebaslebas/log/unicorn.log"
stdout_path "/home/spree/zebaslebas/log/unicorn.log"

# Unicorn socket
listen "/tmp/unicorn.zebaslebas.sock"
#listen "/tmp/unicorn.myapp.sock"

# Number of processes
#worker_processes 4
worker_processes 1

# Time-out
timeout 30
listen "127.0.0.1:8080"

preload_app true

before_fork do |server, worker|
  # Disconnect since the database connection will not carry over
  if defined? ActiveRecord::Base
    ActiveRecord::Base.connection.disconnect!
  end

  if defined?(Resque)
    Resque.redis.quit
    Rails.logger.info('Disconnected from Redis')
  end
end

after_fork do |server, worker|
  # Start up the database connection again in the worker
  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.establish_connection
  end

  if defined?(Resque)
    Resque.redis = ENV['REDIS_URI']
    Rails.logger.info('Connected to Redis')
  end
end
